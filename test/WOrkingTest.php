<?php

namespace Ardith666\Karyawan\Test;

use Ardith666\Karyawan\Test\Karyawan\BackendProgrammer;
use Ardith666\Karyawan\Test\Karyawan\FrontendProgrammer;
use Ardith666\Karyawan\PtiWorking;
use PHPUnit\Framework\TestCase;

class WOrkingTest extends TestCase
{
    public function testKerja()
    {
        $working = new PtiWorking();
        $this->assertEmpty($working->getKaryawan());
    
        $derta = new FrontendProgrammer("derta");
        $agung = new BackendProgrammer("agung");
        
        $working->addKaryawan($derta);
        $working->addKaryawan($agung);
        
        $this->assertEquals(2, count($working->getKaryawan()));
        
        $this->expectOutputRegex('/'.implode(", ", $derta->getTools()).'/');
        $working->mulaiKerja();
    }
}