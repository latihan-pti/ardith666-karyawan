<?php
namespace Ardith666\Karyawan\Test\Karyawan;

use Ardith666\Karyawan\Entity\Karyawan;

class BackendProgrammer extends Karyawan
{
    public function getProjectActive(): string
    {
        return "VMS";
    }
    
    public function getTools(): array
    {
        return [
          "netbean",
          "PHP",
          "Web Browser",
        ];
    }
    
    public function kerja(): string
    {
        return "mulai kerja jam 9 sampai jam 5";
    }
}