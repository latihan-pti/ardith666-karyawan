<?php
namespace Ardith666\Karyawan\Test\Karyawan;

use Ardith666\Karyawan\Entity\Karyawan;

class FrontendProgrammer extends Karyawan
{
    public function getProjectActive(): string
    {
        return "Eproc";
    }
    
    public function getTools(): array
    {
        return [
          "Php Storm",
          "Reac Native",
          "Web Browser",
        ];
    }
    
    public function kerja(): string
    {
        return "mulai kerja jam 9 sampai jam 5";
    }
}