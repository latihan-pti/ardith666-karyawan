<?php

namespace Ardith666\Karyawan\Entity;

abstract class Programmer implements KaryawanInterface
{
    abstract public function getProjectActive(): string;
}