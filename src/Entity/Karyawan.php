<?php


namespace Ardith666\Karyawan\Entity;


abstract class Karyawan implements KaryawanInterface
{
    protected $name;
    
    public function __construct(string $name)
    {
        $this->name = $name;
    }
    
    public function mulaiKerja(): string
    {
        return $this->name . " " . $this->kerja() . " menggunakan tools: " . implode(', ', $this->getTools());
    }
    
    abstract public function getTools(): array;
}