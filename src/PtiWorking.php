<?php

namespace Ardith666\Karyawan;

use Ardith666\Karyawan\Entity\Karyawan;

class PtiWorking
{
    private $karyawan;
    
    public function __construct()
    {
        $this->karyawan = [];
    }
    
    public function addKaryawan(Karyawan $karyawan)
    {
        $this->karyawan[] = $karyawan;
    }
    
    public function getKaryawan()
    {
        return $this->karyawan;
    }
    
    public function mulaiKerja()
    {
        foreach ($this->karyawan as $item) {
            echo $item->mulaiKerja().'<br/>';
        }
    }
}